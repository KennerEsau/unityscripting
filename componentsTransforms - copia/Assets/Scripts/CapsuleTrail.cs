﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTrail : MonoBehaviour {

    private TrailRenderer trail;

    // Use this for initialization
    [Range(1, 50)]
    public float startWidght = 5;
    [Range(1, 50)]
    public float endWidght = 2;
	void Start () {

      if (!(trail = GetComponent<TrailRenderer>())) {

            Debug.LogError("No TraiRederer Component attached to the GameObject.");
            enabled = false;
            return;

        }

    }
	
	// Update is called once per frame
	void Update () {

        GetComponent<TrailRenderer>().time = Random.Range(1, 100);
        GetComponent<TrailRenderer>().startWidth = startWidght;
        GetComponent<TrailRenderer>().endWidth = endWidght;
    }
}
