﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour {

    // Use this for initialization
    public float rotationDegrees = 5f;
    void Start()
    {

        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotation fro Start Event");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotation From Update Event");
    }
}
