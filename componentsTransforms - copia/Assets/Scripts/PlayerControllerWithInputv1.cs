﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame

    public float speed = 5f;
    void Update () {

        
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Debug.Log("key UpArrow pressed");
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            Debug.Log("Key DownArrow Pressed");
            transform.Translate(Vector3.back * Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Debug.Log("key RightArrow pressed");
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            Debug.Log("Key LeftArrow Pressed");
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }



    }
}
