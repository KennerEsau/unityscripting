﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleMod : MonoBehaviour {

    public float scaleUnits = 5f;
  

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.G))
        {
            transform.localScale = new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1,1);
            Debug.Log("Scale from Start Event");
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
       
    }
}
