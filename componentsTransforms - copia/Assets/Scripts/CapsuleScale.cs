﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScale : MonoBehaviour
{

    // Use this for initialization
    public float scaleUnits = 5f;
    void Start()
    {

        transform.localScale = new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
        Debug.Log("Scale fro Start Event");
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
        Debug.Log("Scale from Start Event");
    }
}
